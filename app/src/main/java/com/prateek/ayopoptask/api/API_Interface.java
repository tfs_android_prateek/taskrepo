package com.prateek.ayopoptask.api;


import com.prateek.ayopoptask.models.PhotosPixaBay;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Pagination
 * Created by Prateek on 07/05/18.
 * Copyright (c) 2018. Prateek. All rights reserved.
 */

public interface API_Interface {

    @GET("./")
    Call<PhotosPixaBay> getTopRatedMovies(
            @Query("key") String apiKey,
            @Query("per_page") String language,
            @Query("q") String query,
            @Query("image_type") String imageType,
            @Query("page") int pageIndex);

}
