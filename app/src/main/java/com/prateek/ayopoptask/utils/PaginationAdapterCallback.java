package com.prateek.ayopoptask.utils;

/**
 * Created by Prateek on 07/05/18.
 * Copyright (c) 2018. Prateek. All rights reserved.
 */

public interface PaginationAdapterCallback {

    void retryPageLoad();
}
